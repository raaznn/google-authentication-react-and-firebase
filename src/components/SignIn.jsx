import React, { useState } from "react";
import {
  getAuth,
  signInWithPopup,
  GoogleAuthProvider,
  onAuthStateChanged,
  signOut,
} from "firebase/auth";

const auth = getAuth();

const gProvider = new GoogleAuthProvider();

const handleSignInWithGoogle = () => {
  signInWithPopup(auth, gProvider)
    .then((result) => {
      console.log(result.user);
    })
    .catch((error) => {
      console.log(error.message);
    });

  console.log("called sign in with google");
};

function SignIn() {
  const [isSignedIn, setIsSignedIn] = useState(false);
  const [userName, setUserName] = useState(null);

  onAuthStateChanged(auth, (user) => {
    if (user) {
      setIsSignedIn(true);
      setUserName(user.displayName);
      console.log("SIGNED IN", user.uid);
      // ...
    } else {
      setIsSignedIn(false);
      console.log("SIGNED OUT");
    }
  });

  const handleSignOut = () => {
    signOut(auth)
      .then(() => {
        // Sign-out successful.
      })
      .catch((error) => {
        // An error happened.
      });
  };

  if (isSignedIn)
    return (
      <>
        <h1>Welcome {userName}</h1>
        <button onClick={handleSignOut}>Sign Out</button>
      </>
    );
  else
    return (
      <>
        <button onClick={handleSignInWithGoogle}>Sign in with google</button>
      </>
    );
}

export default SignIn;
